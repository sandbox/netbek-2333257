<?php

/**
 * @file
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Returns a list of machine names of Panels pages implemented by this module.
 *
 * @return array
 */
function panels_code_get_module_pages() {
  $values = variable_get('panels_code_pages', '');

  if ($values === '') {
    return array();
  }

  $arr = explode("\n", $values);
  $parsed = array();

  foreach ($arr as $value) {
    $parts = explode(':', $value);
    list($page_name, $module_name) = $parts;

    $parsed[$value] = array(
      'page_name' => $page_name,
      'module_name' => $module_name,
    );
  }

  return $parsed;
}

/**
 * Returns a list of machine names of Panels task handlers implemented by this
 * module.
 *
 * @return array
 */
function panels_code_get_module_handlers() {
  $values = variable_get('panels_code_handlers', '');

  if ($values === '') {
    return array();
  }

  $arr = explode("\n", $values);
  $parsed = array();

  foreach ($arr as $value) {
    $parts = explode(':', $value);
    list($handler_name, $module_name) = $parts;

    $parsed[$value] = array(
      'handler_name' => $handler_name,
      'module_name' => $module_name,
    );
  }

  return $parsed;
}

/**
 * Deletes a Panels page implemented by this module.
 */
function panels_code_delete_module_page($page) {
  ctools_include('export');
  ctools_include('page', 'page_manager', 'plugins/tasks');

  $loaded_page = page_manager_page_load($page['page_name']);
  if (empty($loaded_page)) {
    drupal_set_message(t('Page <code>!name</code> not found.', array('!name' => $page['page_name'])), 'warning');
    return FALSE;
  }

  page_manager_page_delete($loaded_page);
  return TRUE;
}

/**
 * Deletes all Panels pages implemented by this module.
 */
function panels_code_delete_module_pages() {
  foreach (panels_code_get_module_pages() as $page) {
    panels_code_delete_module_page($page);
  }
  return TRUE;
}

/**
 * Deletes a Panels handler implemented by this module.
 */
function panels_code_delete_module_handler($handler) {
  ctools_include('export');
  ctools_include('page', 'page_manager', 'plugins/tasks');

  $loaded_handler = page_manager_export_task_handler_load($handler['handler_name']);
  if (empty($loaded_handler)) {
    drupal_set_message(t('Handler <code>!name</code> not found.', array('!name' => $handler['handler_name'])), 'warning');
    return FALSE;
  }

  page_manager_delete_task_handler($loaded_handler);
  return TRUE;
}

/**
 * Deletes all Panels handlers implemented by this module.
 */
function panels_code_delete_module_handlers() {
  foreach (panels_code_get_module_handlers() as $handler) {
    panels_code_delete_module_handler($handler);
  }
  return TRUE;
}

/**
 * Returns exportable code for Panels pages and handlers implemented by this
 * module.
 *
 * @param boolean $write_to_file Set to TRUE to write to code to file. This will
 * overwrite the existing file. Requires write permission.
 * @return boolean|array
 */
function panels_code_export_module_pages($write_to_file = FALSE) {
  ctools_include('export');
  ctools_include('page', 'page_manager', 'plugins/tasks');

  $outputs = array();

  foreach (panels_code_get_module_pages() as $page) {
    if (empty($page['module_name']) || !module_exists($page['module_name'])) {
      drupal_set_message(t('Module <code>!name</code> not found.', array('!name' => $page['module_name'])), 'warning');
    }
    else {
      if (!array_key_exists($page['module_name'], $outputs)) {
        $outputs[$page['module_name']] = array(
          'pages' => '',
          'handlers' => '',
        );
      }

      $loaded_page = page_manager_page_load($page['page_name']);
      if (empty($loaded_page)) {
        drupal_set_message(t('Page <code>!name</code> not found.', array('!name' => $page['page_name'])), 'warning');
      }
      else {
        $outputs[$page['module_name']]['pages'] .= trim(page_manager_page_export($loaded_page, TRUE)) . "\n" . '$pages[$page->name] = $page;' . "\n\n";
      }
    }
  }

  foreach (panels_code_get_module_handlers() as $handler) {
    if (empty($handler['module_name']) || !module_exists($handler['module_name'])) {
      drupal_set_message(t('Module <code>!name</code> not found.', array('!name' => $handler['module_name'])), 'warning');
    }
    else {
      if (!array_key_exists($handler['module_name'], $outputs)) {
        $outputs[$page['module_name']] = array(
          'pages' => '',
          'handlers' => '',
        );
      }

      $loaded_handler = page_manager_export_task_handler_load($handler['handler_name']);
      if (empty($loaded_handler)) {
        drupal_set_message(t('Handler <code>!name</code> not found.', array('!name' => $handler['handler_name'])), 'warning');
      }
      else {
        $outputs[$handler['module_name']]['handlers'] .= trim(page_manager_export_task_handler($loaded_handler)) . "\n" . '$handlers[$handler->name] = $handler;' . "\n\n";
      }
    }
  }

  if ($write_to_file) {
    foreach ($outputs as $module_name => $output) {
      $data = '<?php

/**
 * @file
 * Default Panels pages and handlers definitions.
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ' . $module_name . '_default_page_manager_pages() {
  $pages = array();
' . $output['pages'] . '  return $pages;
}

/**
 * Implements hook_default_page_manager_handlers().
 */
function ' . $module_name . '_default_page_manager_handlers() {
  $handlers = array();
' . $output['handlers'] . '  return $handlers;
}
';

      $file_path = panels_code_export_module_pages_path($module_name);

      if (!file_exists($file_path) || is_writable($file_path)) {
        if (file_put_contents($file_path, $data) === FALSE) {
          drupal_set_message(t('Failed to write to <code>!file</code>. Please check the write permissions.', array('!file' => pathinfo($file_path, PATHINFO_BASENAME))), 'error');
          return FALSE;
        }
      }
      else {
        drupal_set_message(t('<code>!file</code> not writable. Please check the write permissions.', array('!file' => pathinfo($file_path, PATHINFO_BASENAME))), 'error');
        return FALSE;
      }
    }

    // @todo Does this work?
    panels_flush_caches();
    return TRUE;
  }

  return $outputs;
}

/**
 * Check if Pages file of a module is writable
 *
 * @param string $module_name
 * @return boolean
 */
function panels_code_export_module_pages_path($module_name) {
  $file_name = $module_name . '.pages_default.inc';
  return drupal_get_path('module', $module_name) . '/pages/' . $file_name;
}

/**
 * Check if Panels pages files of all modules are writable
 *
 * @return boolean|array
 */
function panels_code_export_module_pages_writable() {
  $views = panels_code_get_module_pages();

  if (empty($views)) {
    return TRUE;
  }

  $errors = array();

  foreach ($views as $view) {
    $file_path = panels_code_export_module_pages_path($view['module_name']);

    if (!(!file_exists($file_path) || is_writable($file_path))) {
      $errors[] = pathinfo($file_path, PATHINFO_BASENAME);
    }
  }

  if (empty($errors)) {
    return TRUE;
  }

  return array_unique($errors);
}
