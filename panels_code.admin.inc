<?php

/**
 * @file
 * Admin page callbacks for the Panels Code module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Settings form builder.
 */
function panels_code_admin_settings() {
  $form = array();

  $pages = variable_get('panels_code_pages', '');
  $form['panels_code_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Machine name of pages to manage and export destinations (modules)'),
    '#description' => t('Each value should be on a new line. Format of value: page_name:module_name'),
    '#default_value' => $pages,
  );

  $handlers = variable_get('panels_code_handlers', '');
  $form['panels_code_handlers'] = array(
    '#type' => 'textarea',
    '#title' => t('Machine name of panel handlers to manage and export destinations (modules)'),
    '#description' => t('Each value should be on a new line. Format of value: handler_name:module_name'),
    '#default_value' => $handlers,
  );

  return system_settings_form($form);
}

/**
 * Validation callback for the settings form.
 */
function panels_code_admin_settings_validate($form, &$form_state) {
  foreach (array('panels_code_pages', 'panels_code_handlers') as $field_name) {
    $values = $form_state['values'][$field_name];

    if (!empty($values)) {
      $values = str_replace(array("\r\n", "\r"), "\n", trim($values));
      $arr = explode("\n", $values);
      foreach ($arr as $key => $value) {
        $value = trim($value);
        if ($value === '' || strpos($value, ':') === FALSE) {
          unset($arr[$key]);
        }
        else {
          $arr[$key] = trim($value);
        }
      }
      $arr = array_unique($arr);
      $form_state['values'][$field_name] = implode("\n", $arr);
    }
  }
}

/**
 * Form builder for export form.
 */
function panels_code_admin_export_form($form, &$form_state) {
  if (($writable = panels_code_export_module_pages_writable()) === TRUE) {
    $description = t('Pages and handlers in code and database and those overriding code will be exported.');
  }
  else {
    $description = t('<b>Please grant write permission on <code>!file</code> before exporting.</b>', array('!file' => implode(', ', $writable)));
  }

  $form['export_pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export managed panels pages and handlers to code'),
    '#description' => $description,
  );
  $form['export_pages']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export pages and handlers'),
    '#submit' => array('panels_code_admin_pages_export'),
  );

  return $form;
}

/**
 * Form builder for delete form.
 */
function panels_code_admin_delete_form($form, &$form_state) {
  $form['delete_pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delete managed panels pages'),
    '#description' => t('Pages in database and pages overriding code will be deleted. <b>Pages in code will remain intact.</b>'),
  );
  $form['delete_pages']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete pages'),
    '#submit' => array('panels_code_admin_pages_delete'),
  );

  $form['delete_handlers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delete managed panels handlers'),
    '#description' => t('Handlers in database and handlers overriding code will be deleted. <b>Handlers in code will remain intact.</b>'),
  );
  $form['delete_handlers']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete handlers'),
    '#submit' => array('panels_code_admin_handlers_delete'),
  );

  return $form;
}

/* -----------------------------------------------------------------------------
 * PAGES
 * -------------------------------------------------------------------------- */

/**
 * Form submit handler for export form. Exports pages and handlers.
 */
function panels_code_admin_pages_export() {
  if (panels_code_export_module_pages(TRUE)) {
    $count_pages = count(panels_code_get_module_pages());
    $count_handlers = count(panels_code_get_module_handlers());
    drupal_set_message(t('@pages pages and @handlers handlers processed.', array('@pages' => $count_pages, '@handlers' => $count_handlers)));
  }
  else {
    drupal_set_message(t('The pages and handlers could not be exported to code.'));
  }
}

/**
 * Form submit handler for delete form. Deletes pages.
 */
function panels_code_admin_pages_delete() {
  $operations = array();

  foreach (panels_code_get_module_pages() as $page) {
    $operations[] = array('panels_code_admin_pages_delete_batch', array($page));
  }

  if (empty($operations)) {
    drupal_set_message('There are no pages to delete.');
  }
  else {
    $batch = array(
      'title' => 'Deleting pages',
      'operations' => $operations,
      'finished' => 'panels_code_admin_pages_delete_batch_finished',
      'file' => drupal_get_path('module', 'panels_code') . '/panels_code.admin.inc',
    );
    batch_set($batch);
  }
}

/**
 * Batch operation.
 *
 * @param array $page
 * @param array $context
 */
function panels_code_admin_pages_delete_batch($page, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = 1;
  }

  panels_code_delete_module_page($page);

  $context['results'][] = $page;
  $context['sandbox']['progress'] ++;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback.
 */
function panels_code_admin_pages_delete_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), '@count page processed.', '@count pages processed.');
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while deleting ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }

  drupal_set_message($message);
}

/* -----------------------------------------------------------------------------
 * HANDLERS
 * -------------------------------------------------------------------------- */

/**
 * Form submit handler for delete form. Deletes handlers.
 */
function panels_code_admin_handlers_delete() {
  $operations = array();

  foreach (panels_code_get_module_handlers() as $handler) {
    $operations[] = array('panels_code_admin_handlers_delete_batch', array($handler));
  }

  if (empty($operations)) {
    drupal_set_message('There are no handlers to delete.');
  }
  else {
    $batch = array(
      'title' => 'Deleting handlers',
      'operations' => $operations,
      'finished' => 'panels_code_admin_handlers_delete_batch_finished',
      'file' => drupal_get_path('module', 'panels_code') . '/panels_code.admin.inc',
    );
    batch_set($batch);
  }
}

/**
 * Batch operation.
 *
 * @param array $handler
 * @param array $context
 */
function panels_code_admin_handlers_delete_batch($handler, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = 1;
  }

  panels_code_delete_module_handler($handler);

  $context['results'][] = $handler;
  $context['sandbox']['progress'] ++;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback.
 */
function panels_code_admin_handlers_delete_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), '@count handler processed.', '@count handlers processed.');
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while deleting ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }

  drupal_set_message($message);
}
